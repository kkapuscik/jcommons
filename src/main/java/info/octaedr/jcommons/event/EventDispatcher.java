/*------------------------------------------------------------------------------
- This file is part of OCTjcommons project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jcommons.event;

import java.util.Collection;
import java.util.EventListener;
import java.util.EventObject;

/**
 * Dispatcher for events.
 *
 * @param <ListenerType>
 *            Type of the listener.
 * @param <EventType>
 *            Type of the event.
 */
public abstract class EventDispatcher<ListenerType extends EventListener, EventType extends EventObject> {

    /**
     * Constructs event dispatcher.
     */
    protected EventDispatcher() {
        // nothing to do
    }

    /**
     * Schedules event to be dispatched.
     *
     * @param listener
     *            Listener to be notified.
     * @param event
     *            Event object.
     */
    public abstract void scheduleEvent(ListenerType listener, EventType event);

    /**
     * Schedules event to be dispatched.
     *
     * @param listeners
     *            Array of listeners to be notified.
     * @param event
     *            Event object.
     */
    public void scheduleEvent(final ListenerType[] listeners, final EventType event) {
        for (final ListenerType listener : listeners) {
            scheduleEvent(listener, event);
        }
    }

    /**
     * Schedules event to be dispatched.
     *
     * @param listeners
     *            Collection of listeners to be notified.
     * @param event
     *            Event object.
     */
    public void scheduleEvent(final Collection<ListenerType> listeners, final EventType event) {
        for (final ListenerType listener : listeners) {
            scheduleEvent(listener, event);
        }
    }

    /**
     * Notifies listener about an event.
     *
     * @param listener
     *            Listener to be notifier.
     * @param event
     *            Event object.
     */
    protected abstract void notifyEvent(ListenerType listener, EventType event);

}
