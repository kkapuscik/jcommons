/*------------------------------------------------------------------------------
- This file is part of OCTjcommons project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jcommons.event;

import java.util.EventListener;
import java.util.EventObject;

/**
 * Synchronous event dispatcher.
 *
 * <p>
 * Synchronous dispatcher immediately calls event notification from the thread that called one of
 * schedule methods (e.g. {@link #scheduleEvent(EventListener, EventObject)}.
 * </p>
 *
 * @param <ListenerType>
 *            Type of the listener.
 * @param <EventType>
 *            Type of the event.
 */
public abstract class SynchronousEventDispatcher<ListenerType extends EventListener, EventType extends EventObject>
extends EventDispatcher<ListenerType, EventType> {

    /**
     * Constructs synchronous dispatcher.
     */
    public SynchronousEventDispatcher() {
        // nothing to do
    }

    @Override
    public void scheduleEvent(final ListenerType listener, final EventType event) {
        notifyEvent(listener, event);
    }

}
