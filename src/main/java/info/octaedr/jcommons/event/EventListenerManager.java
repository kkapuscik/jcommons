/*------------------------------------------------------------------------------
- This file is part of OCTjcommons project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jcommons.event;

import info.octaedr.jcommons.collection.WriteSafeHashSet;

import java.util.EventListener;
import java.util.EventObject;

/**
 * Thread-safe manager for EventListeners.
 *
 * @param <ListenerType>
 *            Type of listeners managed.
 * @param <EventType>
 *            Type of events dispatched.
 */
public class EventListenerManager<ListenerType extends EventListener, EventType extends EventObject> {

    /** Collection of registered listeners. */
    private final WriteSafeHashSet<ListenerType> listenerCollection;
    /** Event dispatcher. */
    private final EventDispatcher<ListenerType, EventType> dispatcher;

    /**
     * Constructs new manager with.
     *
     * @param dispatcher
     *            Event dispatcher.
     */
    protected EventListenerManager(final EventDispatcher<ListenerType, EventType> dispatcher) {
        super();

        if (dispatcher == null) {
            throw new NullPointerException("Null dispatcher given");
        }

        this.dispatcher = dispatcher;
        this.listenerCollection = new WriteSafeHashSet<ListenerType>();
    }

    /**
     * Schedules event to be dispatched.
     *
     * @param listener
     *            Listener to be notified.
     * @param event
     *            Event object.
     */
    protected void scheduleEventUnsafe(final ListenerType listener, final EventType event) {
        this.dispatcher.scheduleEvent(listener, event);
    }

    /**
     * Adds listener.
     *
     * @param listener
     *            Listener to be added. If listener is already managed does nothing.
     */
    public synchronized void addListener(final ListenerType listener) {
        this.listenerCollection.add(listener);
    }

    /**
     * Removes listener.
     *
     * @param listener
     *            Listener to be removed. If listener is not manager does nothing.
     */
    public synchronized void removeListener(final ListenerType listener) {
        this.listenerCollection.remove(listener);
    }

    /**
     * Schedules event to be dispatched to listeners.
     *
     * @param event
     *            Event object.
     */
    public synchronized void scheduleEvent(final EventType event) {
        this.dispatcher.scheduleEvent(this.listenerCollection.getContents(), event);
    }

}
