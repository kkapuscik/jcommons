/*------------------------------------------------------------------------------
- This file is part of OCTjcommons project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jcommons.event;

import info.octaedr.jcommons.logging.Logger;

import java.util.EventListener;
import java.util.EventObject;
import java.util.LinkedList;

/**
 * Queued event dispatcher.
 *
 * <p>
 * Queued dispatcher adds scheduled events into a queue. Then, in separate task it calls the
 * listeners in the same order as the events were scheduled.
 * </p>
 *
 * @param <ListenerType>
 *            Type of the listener.
 * @param <EventType>
 *            Type of the event.
 */
public abstract class QueuedEventDispatcher<ListenerType extends EventListener, EventType extends EventObject>
        extends EventDispatcher<ListenerType, EventType> {

    /** Logger for printing debug messages. */
    private static final Logger logger = Logger.getLogger(QueuedEventDispatcher.class);

    /**
     * Event queue entry.
     */
    private class Entry {

        /** Listener to be notified. */
        public final ListenerType listener;
        /** Event object. */
        public final EventType event;

        /**
         * Constructs event queue entry.
         *
         * @param listener
         *            Listener to be notified.
         * @param event
         *            Event object.
         */
        public Entry(final ListenerType listener, final EventType event) {
            this.listener = listener;
            this.event = event;
        }

    }

    /**
     * Thread for sending the events from queue.
     */
    private class EventQueueThread extends Thread {

        @Override
        public void run() {
            while (QueuedEventDispatcher.this.processNextEvent()) {
                // nothing to do
            }
        }

    }

    /** Default value for thread idle time. */
    private static final int DEFAULT_THREAD_IDLE_TIME = 3 * 1000;

    /** Event queue thread. */
    private EventQueueThread executionThread;

    /** Queue with events. */
    private final LinkedList<Entry> eventQueue = new LinkedList<Entry>();

    /**
     * Time in milliseconds the event could stay in idle state (the state when there are no events
     * to send).
     */
    private final int threadIdleTime;

    /**
     * Constructs synchronous dispatcher.
     */
    public QueuedEventDispatcher() {
        this(DEFAULT_THREAD_IDLE_TIME);
    }

    /**
     * Constructs synchronous dispatcher.
     *
     * @param threadIdleTime
     *            Time in milliseconds the event could stay in idle state (the state when there are
     *            no events to send).
     */
    public QueuedEventDispatcher(final int threadIdleTime) {
        if (threadIdleTime < 0) {
            throw new IllegalArgumentException("Given thread idle time is negative: "
                    + threadIdleTime);
        }

        this.threadIdleTime = threadIdleTime;
    }

    @Override
    public void scheduleEvent(final ListenerType listener, final EventType event) {
        if (listener == null) {
            throw new NullPointerException("Null listener given");
        }
        if (event == null) {
            throw new NullPointerException("Null event given");
        }

        /* create entry */
        final Entry entry = new Entry(listener, event);

        synchronized (this) {
            /* create thread if there is none */
            if (this.executionThread == null) {
                this.executionThread = new EventQueueThread();
                this.executionThread.start();
            }

            this.eventQueue.add(entry);
        }
    }

    /**
     * Processes next event in queue (if any available).
     *
     * @return True if thread shall stay alive (to process next events), false to terminate the
     *         thread.
     */
    private boolean processNextEvent() {
        Entry entry = null;

        synchronized (this) {
            if (this.eventQueue.isEmpty()) {
                try {
                    wait(this.threadIdleTime);
                } catch (final InterruptedException e) {
                    // ignore
                }
            }

            if (!this.eventQueue.isEmpty()) {
                entry = this.eventQueue.removeFirst();
            } else {
                this.executionThread = null;
            }
        }

        if (entry != null) {
            try {
                notifyEvent(entry.listener, entry.event);
            } catch (final Throwable t) {
                if (logger.trace()) {
                    logger.trace("Device description failed.", t);
                }
            }

            /* the thread shall stay alive */
            return true;
        } else {
            /* let the thread die */
            return false;
        }
    }

}
