package info.octaedr.jcommons.string;

import java.util.ArrayList;

/**
 * Utilities for string manipulation.
 */
public class StringTools {

    /**
     * Forbid creating instances.
     */
    private StringTools() {
        // nothing to do
    }

    /**
     * Splits string into tokens.
     *
     * @param str
     *            String to be split.
     * @param delim
     *            Delimited character.
     * @param removeEmpty
     *            If true the empty elements will not be returned.
     *
     * @return String tokens.
     */
    public static String[] splitString(final String str, final char delim, final boolean removeEmpty) {
        if (str == null) {
            throw new NullPointerException("Null string given");
        }

        final ArrayList<String> tokens = new ArrayList<String>();

        final int len = str.length();
        int start = 0;
        for (int i = 0; i < len; ++i) {
            final char c = str.charAt(i);
            if (c == delim) {
                /* we have a new string */
                final int tokenLen = i - start;

                /* check if we want to store the token */
                if ((tokenLen > 0) || !removeEmpty) {
                    tokens.add(str.substring(start, i));
                }

                /* the new token starts here */
                start = i + 1;
            }
        }

        /* check if we want to store the last token */
        final int lastTokenLen = len - start;
        if ((lastTokenLen > 0) || !removeEmpty) {
            tokens.add(str.substring(start));
        }

        /* convert to array */
        return tokens.toArray(new String[tokens.size()]);
    }

}
