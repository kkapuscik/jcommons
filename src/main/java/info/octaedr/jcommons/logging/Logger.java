/*------------------------------------------------------------------------------
- This file is part of OCTjcommons project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jcommons.logging;

import java.util.MissingResourceException;
import java.util.logging.Level;

/**
 * Logger for printing debug messages.
 *
 * <p>
 * It is a wrapper over {@link java.util.logging.Logger}.
 * </p>
 */
public class Logger {

    /**
     * Gets a named logger. The returned logger may already exist or may be newly created. In the
     * latter case, its level will be set to the configured level according to the LogManager's
     * properties.
     *
     * @param clazz
     *            Class for which logger is created. The name of created logger will be
     *            clazz.getName().
     *
     * @return Created named logger.
     *
     * @throws MissingResourceException
     *             If the specified resource bundle can not be loaded.
     */
    public static Logger getLogger(final Class<?> clazz) {
        final java.util.logging.Logger internalLogger = java.util.logging.Logger.getLogger(clazz
                .getName());
        return new Logger(internalLogger);
    }

    /** Internal (real) logger. */
    private final java.util.logging.Logger internalLogger;

    /**
     * Constructs logger.
     *
     * @param internalLogger
     *            Standard java logger to be used to log messages.
     */
    private Logger(final java.util.logging.Logger internalLogger) {
        if (internalLogger == null) {
            throw new NullPointerException("Null internal logger given");
        }
        this.internalLogger = internalLogger;
    }

    /**
     * Checks if logging with level SEVERE is enabled.
     *
     * @return True if logging is enabled, false otherwise.
     */
    public boolean severe() {
        return this.internalLogger.isLoggable(Level.SEVERE);
    }

    /**
     * Checks if logging with level WARNING is enabled.
     *
     * @return True if logging is enabled, false otherwise.
     */
    public boolean warning() {
        return this.internalLogger.isLoggable(Level.WARNING);
    }

    /**
     * Checks if logging with level INFO is enabled.
     *
     * @return True if logging is enabled, false otherwise.
     */
    public boolean info() {
        return this.internalLogger.isLoggable(Level.INFO);
    }

    /**
     * Checks if logging with level CONFIG is enabled.
     *
     * @return True if logging is enabled, false otherwise.
     */
    public boolean config() {
        return this.internalLogger.isLoggable(Level.CONFIG);
    }

    /**
     * Checks if logging with level FINE is enabled.
     *
     * <p>
     * This method is an alias for {@link #fine()}.
     * </p>
     *
     * @return True if logging is enabled, false otherwise.
     */
    public boolean trace() {
        return this.internalLogger.isLoggable(Level.FINE);
    }

    /**
     * Checks if logging with level FINE is enabled.
     *
     * @return True if logging is enabled, false otherwise.
     */
    public boolean fine() {
        return this.internalLogger.isLoggable(Level.FINE);
    }

    /**
     * Checks if logging with level FINER is enabled.
     *
     * @return True if logging is enabled, false otherwise.
     */
    public boolean finer() {
        return this.internalLogger.isLoggable(Level.FINER);
    }

    /**
     * Checks if logging with level FINEST is enabled.
     *
     * @return True if logging is enabled, false otherwise.
     */
    public boolean finest() {
        return this.internalLogger.isLoggable(Level.FINEST);
    }

    /**
     * Logs a SEVERE message.
     *
     * @param msg
     *            The message to be logged.
     *
     * @see java.util.logging.Logger#severe(String)
     */
    public void severe(final String msg) {
        this.internalLogger.severe(msg);
    }

    /**
     * Logs a WARNING message.
     *
     * @param msg
     *            The message to be logged.
     *
     * @see java.util.logging.Logger#warning(String)
     */
    public void warning(final String msg) {
        this.internalLogger.warning(msg);
    }

    /**
     * Logs a INFO message.
     *
     * @param msg
     *            The message to be logged.
     *
     * @see java.util.logging.Logger#info(String)
     */
    public void info(final String msg) {
        this.internalLogger.info(msg);
    }

    /**
     * Logs a CONFIG message.
     *
     * @param msg
     *            The message to be logged.
     *
     * @see java.util.logging.Logger#config(String)
     */
    public void config(final String msg) {
        this.internalLogger.config(msg);
    }

    /**
     * Logs atrace (FINE alias) message.
     *
     * @param msg
     *            The message to be logged.
     *
     * @see Logger#fine()
     */
    public void trace(final String msg) {
        this.internalLogger.fine(msg);
    }

    /**
     * Logs aFINE message.
     *
     * @param msg
     *            The message to be logged.
     *
     * @see Logger#fine()
     */
    public void fine(final String msg) {
        this.internalLogger.fine(msg);
    }

    /**
     * Logs aFINER message.
     *
     * @param msg
     *            The message to be logged.
     *
     * @see Logger#finer()
     */
    public void finer(final String msg) {
        this.internalLogger.finer(msg);
    }

    /**
     * Logs aFINEST message.
     *
     * @param msg
     *            The message to be logged.
     *
     * @see Logger#finest()
     */
    public void finest(final String msg) {
        this.internalLogger.finest(msg);
    }

    /**
     * Logs a SEVERE message.
     *
     * @param msg
     *            The message to be logged.
     * @param thrown
     *            Thrown object to be logged.
     *
     * @see java.util.logging.Logger#severe(String)
     */
    public void severe(final String msg, final Throwable thrown) {
        this.internalLogger.log(Level.SEVERE, msg, thrown);
    }

    /**
     * Logs a WARNING message.
     *
     * @param msg
     *            The message to be logged.
     * @param thrown
     *            Thrown object to be logged.
     *
     * @see java.util.logging.Logger#warning(String)
     */
    public void warning(final String msg, final Throwable thrown) {
        this.internalLogger.log(Level.WARNING, msg, thrown);
    }

    /**
     * Logs a INFO message.
     *
     * @param msg
     *            The message to be logged.
     * @param thrown
     *            Thrown object to be logged.
     *
     * @see java.util.logging.Logger#info(String)
     */
    public void info(final String msg, final Throwable thrown) {
        this.internalLogger.log(Level.INFO, msg, thrown);
    }

    /**
     * Logs a CONFIG message.
     *
     * @param msg
     *            The message to be logged.
     * @param thrown
     *            Thrown object to be logged.
     *
     * @see java.util.logging.Logger#config(String)
     */
    public void config(final String msg, final Throwable thrown) {
        this.internalLogger.log(Level.CONFIG, msg, thrown);
    }

    /**
     * Logs atrace (FINE alias) message.
     *
     * @param msg
     *            The message to be logged.
     * @param thrown
     *            Thrown object to be logged.
     *
     * @see Logger#fine()
     */
    public void trace(final String msg, final Throwable thrown) {
        this.internalLogger.log(Level.FINE, msg, thrown);
    }

    /**
     * Logs aFINE message.
     *
     * @param msg
     *            The message to be logged.
     * @param thrown
     *            Thrown object to be logged.
     *
     * @see Logger#fine()
     */
    public void fine(final String msg, final Throwable thrown) {
        this.internalLogger.log(Level.FINE, msg, thrown);
    }

    /**
     * Logs aFINER message.
     *
     * @param msg
     *            The message to be logged.
     * @param thrown
     *            Thrown object to be logged.
     *
     * @see Logger#finer()
     */
    public void finer(final String msg, final Throwable thrown) {
        this.internalLogger.log(Level.FINER, msg, thrown);
    }

    /**
     * Logs aFINEST message.
     *
     * @param msg
     *            The message to be logged.
     * @param thrown
     *            Thrown object to be logged.
     *
     * @see Logger#finest()
     */
    public void finest(final String msg, final Throwable thrown) {
        this.internalLogger.log(Level.FINEST, msg, thrown);
    }

}
