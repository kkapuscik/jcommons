/*------------------------------------------------------------------------------
- This file is part of OCTjcommons project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jcommons.logging;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

/**
 * Default log messages formatter.
 */
public class DefaultFormatter extends Formatter {

    @Override
    public String format(final LogRecord record) {
        final StringBuilder buffer = new StringBuilder();

        buffer.append(record.getMillis()).append(' ');
        buffer.append(record.getLevel()).append(' ');
        buffer.append('[').append(record.getLoggerName()).append(']').append(' ');
        buffer.append(record.getMessage()).append('\n');

        final Throwable t = record.getThrown();
        if (t != null) {
            final StringWriter writer = new StringWriter();
            t.printStackTrace(new PrintWriter(writer));
            buffer.append(writer.toString());
            buffer.append('\n');
        }

        return buffer.toString();
    }

}
