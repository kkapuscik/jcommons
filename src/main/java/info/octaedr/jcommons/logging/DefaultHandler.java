/*------------------------------------------------------------------------------
- This file is part of OCTjcommons project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jcommons.logging;

import java.util.logging.ConsoleHandler;

/**
 * Default handler that prints messages using System.out.
 */
public class DefaultHandler extends ConsoleHandler {

    /**
     * Constructs new handler.
     */
    public DefaultHandler() {
        super();
        setOutputStream(System.out);
    }

}
