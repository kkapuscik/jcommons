/*------------------------------------------------------------------------------
- This file is part of OCTjcommons project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jcommons.logging;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Log manager.
 */
public class LogManager {

    /**
     * Initializes logging using default settings.
     *
     * <p>
     * Default settings:
     * <ul>
     * <li>info.octaedr.* loggers level set to Level.ALL
     * <li>Default handler constructed and configured to print Level.ALL messages.
     * <li>Default formatter assigned to default handler.
     * </ul>
     */
    public static void init() {
        final Logger logger = Logger.getLogger("info.octaedr");

        /* remove all old handlers */
        for (final Handler handler : logger.getHandlers()) {
            logger.removeHandler(handler);
        }

        /* add new handler */
        final ConsoleHandler handler = new DefaultHandler();
        handler.setLevel(Level.ALL);
        handler.setFormatter(new DefaultFormatter());
        logger.addHandler(handler);

        /* setup logger */
        logger.setLevel(Level.ALL);
    }

    /**
     * Initializes logging using configuration file from resources.
     *
     * @param resourceName
     *            Name of the configuration file.
     */
    public static void init(final String resourceName) {
        InputStream configStream = null;
        boolean initSuccess = false;

        try {
            configStream = Logger.class.getClassLoader().getResourceAsStream(resourceName);
            if (configStream != null) {
                java.util.logging.LogManager.getLogManager().readConfiguration(configStream);
                initSuccess = true;
            }
        } catch (final Exception e) {
            e.printStackTrace();
        } finally {
            if (!initSuccess) {
                /* initialize using defaults */
                init();
            }

            if (configStream != null) {
                try {
                    configStream.close();
                } catch (final IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
