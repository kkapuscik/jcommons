/*------------------------------------------------------------------------------
- This file is part of OCTjcommons project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jcommons.collection;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Write safe collection using ArrayList to store elements.
 *
 * @param <ElementType>
 *            Type of the elements in collection.
 */
public class WriteSafeArrayList<ElementType> extends WriteSafeCollection<ElementType> {

    /**
     * Constructs collection.
     */
    public WriteSafeArrayList() {
        super();
    }

    @Override
    protected Collection<ElementType> createCollection() {
        return new ArrayList<ElementType>();
    }

    @Override
    protected Collection<ElementType> cloneCollection(final Collection<ElementType> collection,
            final int sizeChange) {
        final ArrayList<ElementType> newCollection = new ArrayList<ElementType>(collection.size()
                + sizeChange);
        newCollection.addAll(collection);
        return newCollection;
    }

}
