/*------------------------------------------------------------------------------
- This file is part of OCTjcommons project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jcommons.collection;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

/**
 * Write-safe collection.
 *
 * <p>
 * The main idea is that there are much more read accesses than write accesses.
 *
 * <ul>
 * <li>The contents of collection are available at any time without need of copying. The collection
 * is returned directly.
 * <li>During collection modification the current contents are copied, then the change is performed
 * and the modification result is stored as current value of the collection.
 * </ul>
 * </p>
 *
 * @param <ElementType>
 *            Type of the element in connection.
 */
public abstract class WriteSafeCollection<ElementType> implements Iterable<ElementType> {

    /** Collection contents (read/write access). */
    private Collection<ElementType> writeCollection;
    /** Wrapped collection (read access only). */
    private Collection<ElementType> readCollection;

    /**
     * Constructs new manager with.
     */
    public WriteSafeCollection() {
        this.writeCollection = createCollection();
        this.readCollection = Collections.unmodifiableCollection(this.writeCollection);
    }

    @Override
    public Iterator<ElementType> iterator() {
        return getContents().iterator();
    }

    /**
     * Adds object to collection.
     *
     * @param object
     *            Object to be added.
     *
     * @see Collection#add(Object)
     */
    public synchronized void add(final ElementType object) {
        if (object == null) {
            throw new NullPointerException("Null object given");
        }

        if (!this.writeCollection.contains(object)) {
            final Collection<ElementType> newCollection = cloneCollection(this.writeCollection, +1);
            newCollection.add(object);
            this.writeCollection = newCollection;
            this.readCollection = Collections.unmodifiableCollection(this.writeCollection);
        }
    }

    /**
     * Removes object from collection.
     *
     * @param object
     *            Object to be removed.
     *
     * @see Collection#remove(Object)
     */
    public synchronized void remove(final ElementType object) {
        if (object == null) {
            throw new NullPointerException("Null object given");
        }

        if (this.writeCollection.contains(object)) {
            final Collection<ElementType> newCollection = cloneCollection(this.writeCollection, 0);
            newCollection.remove(object);
            this.writeCollection = newCollection;
            this.readCollection = Collections.unmodifiableCollection(this.writeCollection);
        }
    }

    /**
     * Returns current collection contents.
     *
     * @return Collection contents. The returned collection could not be modified (it is write
     *         protected).
     */
    public synchronized Collection<ElementType> getContents() {
        return this.readCollection;
    }

    /**
     * Constructs collection.
     *
     * @return Empty collection.
     */
    protected abstract Collection<ElementType> createCollection();

    /**
     * Clones given collection..
     *
     * @param collection
     *            Collection to be cloned. It could be collection returned from
     *            {@link #createCollection()} or from previous call to
     *            {@link #cloneCollection(Collection, int)}
     * @param sizeGrow
     *            Estimated change of the size during next collection modification. If shall be
     *            non-negative.
     *
     * @return Cloned collection.
     */
    protected abstract Collection<ElementType> cloneCollection(Collection<ElementType> collection,
            int sizeGrow);

}
