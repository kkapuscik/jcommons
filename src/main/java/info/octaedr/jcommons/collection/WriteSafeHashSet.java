/*------------------------------------------------------------------------------
- This file is part of OCTjcommons project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jcommons.collection;

import java.util.Collection;
import java.util.HashSet;

/**
 * Write safe collection using HashSet to store elements.
 *
 * @param <ElementType>
 *            Type of the elements in collection.
 */
public class WriteSafeHashSet<ElementType> extends WriteSafeCollection<ElementType> {

    /**
     * Constructs collection.
     */
    public WriteSafeHashSet() {
        super();
    }

    @Override
    protected Collection<ElementType> createCollection() {
        return new HashSet<ElementType>();
    }

    @Override
    protected Collection<ElementType> cloneCollection(final Collection<ElementType> collection,
            final int sizeChange) {
        final HashSet<ElementType> newCollection = new HashSet<ElementType>(collection.size()
                + sizeChange);
        newCollection.addAll(collection);
        return newCollection;
    }

}
