/*------------------------------------------------------------------------------
- This file is part of OCTjcommons project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jcommons.collection;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Iterator over read-only array.
 *
 * @param <E>
 *            Type of elements in array.
 */
class ReadOnlyArrayIterator<E> implements Iterator<E> {

    /**
     * Wrapped array.
     */
    private final E[] array;

    /**
     * Index of elements iterator currently points at.
     */
    private int index;

    /**
     * Constructs new iterator.
     *
     * @param array
     *            Array for which the iterator is created.
     */
    public ReadOnlyArrayIterator(final E[] array) {
        if (array == null) {
            throw new NullPointerException("Null array given");
        }

        this.array = array;
        this.index = 0;
    }

    @Override
    public boolean hasNext() {
        return this.index < this.array.length;
    }

    @Override
    public E next() {
        if (!hasNext()) {
            throw new NoSuchElementException("Index: " + this.index + " - Size: "
                    + this.array.length);
        }
        return this.array[this.index++];
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

}
