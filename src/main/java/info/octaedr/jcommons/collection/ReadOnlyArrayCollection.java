/*------------------------------------------------------------------------------
- This file is part of OCTjcommons project.
-
- Copyright (C) 2008-2012 Krzysztof Kapuscik (OCTaedr)
- All rights reserved.
------------------------------------------------------------------------------*/

package info.octaedr.jcommons.collection;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

/**
 * Collection wrapping read-only array.
 *
 * @param <E>
 *            Type of the elements in array.
 */
public class ReadOnlyArrayCollection<E> implements Collection<E> {

    /**
     * The wrapped array.
     */
    private final E[] array;

    /**
     * Constructs new collection.
     *
     * @param array
     *            Array to be wrapped.
     */
    public ReadOnlyArrayCollection(final E[] array) {
        if (array == null) {
            throw new NullPointerException("Null array given");
        }

        this.array = array;
    }

    @Override
    public int size() {
        return this.array.length;
    }

    @Override
    public boolean isEmpty() {
        return this.array.length == 0;
    }

    @Override
    public boolean contains(final Object o) {
        for (final E item : this.array) {
            if (item.equals(o)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean containsAll(final Collection<?> other) {
        for (final Object o : other) {
            if (!contains(o)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Iterator<E> iterator() {
        return new ReadOnlyArrayIterator<E>(this.array);
    }

    @Override
    public Object[] toArray() {
        return toArray(new Object[this.array.length]);
    }

    @Override
    public <T> T[] toArray(final T[] other) {
        final int toCopy = Math.min(this.array.length, other.length);

        System.arraycopy(this.array, 0, other, 0, toCopy);
        if (toCopy < other.length) {
            Arrays.fill(other, toCopy, other.length, null);
        }

        return other;
    }

    @Override
    public boolean add(final E e) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean remove(final Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(final Collection<? extends E> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAll(final Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean retainAll(final Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException();
    }

}
