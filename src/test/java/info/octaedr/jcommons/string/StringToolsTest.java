/**
 *
 */

package info.octaedr.jcommons.string;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;

import org.junit.Test;

/**
 * Test for StringTools class.
 */
public class StringToolsTest {

    @Test
    public void testConstructorIsPrivate() throws Exception {
        final Constructor<StringTools> constructor = StringTools.class.getDeclaredConstructor();
        assertTrue(Modifier.isPrivate(constructor.getModifiers()));
        constructor.setAccessible(true);
        final Object o = constructor.newInstance();
        assertNotNull(o);
    }

    @Test(expected = NullPointerException.class)
    public void testNullString() throws Exception {
        StringTools.splitString(null, ':', true);
    }

    @Test
    public void testSplitString() {
        final String input1 = "ala ma kota";
        final String[] output1 = { "ala", "ma", "kota" };
        final String[] result1 = StringTools.splitString(input1, ' ', false);
        assertArrayEquals(output1, result1);

        final String input2 = "a bbb c";
        final String[] output2 = { "a", "bbb", "c" };
        final String[] result2 = StringTools.splitString(input2, ' ', false);
        assertArrayEquals(output2, result2);

        final String input3 = "a  bbb  c ddd";
        final String[] output3 = { "a", "bbb", "c", "ddd" };
        final String[] result3 = StringTools.splitString(input3, ' ', true);
        assertArrayEquals(output3, result3);

        final String input4 = "a,";
        final String[] output4 = { "a" };
        final String[] result4 = StringTools.splitString(input4, ',', true);
        assertArrayEquals(output4, result4);

        final String input5 = "a,";
        final String[] output5 = { "a", "" };
        final String[] result5 = StringTools.splitString(input5, ',', false);
        assertArrayEquals(output5, result5);

        final String input6 = ",a";
        final String[] output6 = { "a" };
        final String[] result6 = StringTools.splitString(input6, ',', true);
        assertArrayEquals(output6, result6);

        final String input7 = ",a";
        final String[] output7 = { "", "a" };
        final String[] result7 = StringTools.splitString(input7, ',', false);
        assertArrayEquals(output7, result7);

        final String input8 = ",a,";
        final String[] output8 = { "a" };
        final String[] result8 = StringTools.splitString(input8, ',', true);
        assertArrayEquals(output8, result8);

        final String input9 = ",a,";
        final String[] output9 = { "", "a", "" };
        final String[] result9 = StringTools.splitString(input9, ',', false);
        assertArrayEquals(output9, result9);
    }

}
